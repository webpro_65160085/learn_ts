const car: {type: string,model : string,year?: number} = {
    type: "Toyota",
    model: "Corolla"
};
//if have (?) it be optional such as year type

console.log(car);

car.type = "Suzuki"
car.model = "Swift"
console.log(car);
